#!/usr/bin/python3
import PyCmdMessenger #https://github.com/harmsm/PyCmdMessenger
import time
import numpy

arduino = PyCmdMessenger.ArduinoBoard("/dev/ttyUSB0",baud_rate=14400)
# Baud Rates: 9600, 14400, 19200
colors = {"RED":0, "ORANGE":32, "YELLOW":64, "GREEN":96, "BLUE":160, "PURPLE":192, "PINK":224}

# List of commands and their associated argument formats. These must be in the
# same order as in the sketch.
commands = [["LED_ON","iii"],
            ["LEDS_OFF",""],
            ["DIS_NUM","ii"],
            ["CALI","i"],
            ["error","s"],
            ["return_str","s"]]

return_messages = {"LED_ON":"LED_SET",
                   "LEDS_OFF":"LEDS_OFF",
                   "DIS_NUM":"NUM_SET",
                   "CALI":"CALIB"
                   }


def LED_ON(num, color, bright): # Change x LED
    check = 0
    c.send("LED_ON",num, colors[color],bright)
    while check == 0:
        #print("LED set")
        check = messsage_check("LED_ON")

def LEDS_OFF(): # Turns off all LEDS
    check = 0
    c.send("LEDS_OFF")
    while check == 0:
        #print("LEDs off")
        check = messsage_check("LEDS_OFF")

def DIS_NUM(number, obj):
    check = 0
    c.send("DIS_NUM", number, obj)
    while check == 0:
        check = messsage_check("DIS_NUM")

def CALI(stepper):
    check = 0
    c.send("CALI", stepper)
    while check == 0:
        check = messsage_check("CALI")

def messsage_check(command):
    msg = c.receive()
    print(msg)
    if msg is not None:
        print("not None")
        if msg[1][0] == return_messages[command]:
            print(msg)
            print("return 1")
            return 1
    print("return 0")
    return 0
    print(msg)

def error(msg):
    print("python error: " + msg)

c = PyCmdMessenger.CmdMessenger(arduino,commands) # Initialize the messenger
a = numpy.random.randint(1,7)
b = 1
n = 1
start = 0
#c.send("Rasp_On") # let arduino know raspberry is fully booted
#LED_ON(0, "GREEN",40)
#time.sleep(5)
#LEDS_OFF()
CALI(1)
CALI(2)
CALI(3)
DIS_NUM(3,1)
DIS_NUM(10,2)
DIS_NUM(4,3)
