#Moon phase
#https://github.com/codebox/lunar-calendar
#https://gist.github.com/miklb/ed145757971096565723

#sun rise/set/dawn/dusk/moon phase & more
#https://github.com/sffjunkie/astral/

#https://github.com/geopy/geopy

#geo ip location (offline)
#https://github.com/maxmind/GeoIP2-python
#https://dev.maxmind.com/geoip/geoip2/geolite2/
#https://dev.maxmind.com/geoip/geoipupdate/

#In use
#https://developer.yahoo.com/weather/documentation.html
#ttps://www.yahoo.com/news/weather

#https://pypi.org/project/pgeocode/
#woeid look up : http://woeid.rosselliot.co.nz/
#https://www.geeksforgeeks.org/python-pandas-dataframe/

from yahoo_weather.weather import YahooWeather #https://pypi.org/project/yahoo-weather/
from yahoo_weather.config.units import Unit
import pgeocode
import pandas as pd
import numpy as np

#zip code location lookup
#The result of a geo-localistion query is a pandas.DataFrame
nomi = pgeocode.Nominatim('us')
a = nomi.query_postal_code("98584")
print(a.place_name)

weather = YahooWeather(APP_ID="0AoxXi48",
                     api_key="dj0yJmk9QzVHUzJGVU1ZOWsxJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PWUz",
                     api_secret="0ad2db4b142fe67eb9269d7f29b2e2602bf9a950")

#weather.get_yahoo_weather_by_city("shelton, wa", Unit.fahrenheit)
weather.get_yahoo_weather_by_location(a.loc["latitude"], a.loc["longitude"], Unit.fahrenheit)

condition = {"text": weather.condition.text, "code": weather.condition.code, "temperature": weather.condition.temperature}
astronomy = {"sunrise": weather.astronomy.sunrise, "sunset": weather.astronomy.sunset}
atmosphere = {"humidity": weather.atmosphere.humidity, "pressure": weather.atmosphere.pressure, "rising": weather.atmosphere.rising, "visibility": weather.atmosphere.visibility}
wind = {"chill": weather.wind.chill,"direction": weather.wind.direction, "speed": weather.wind.speed}
print(condition)
print(astronomy)
print(atmosphere)
print(wind)
