/* need to remove Adafruit-MAX31855 dependency from library.json file in the
Arduino-CmdMessenger folder to have lib work */
#include "CmdMessenger.h"   // https://github.com/thijse/Arduino-CmdMessenger
#include <FastLED.h>        // https://github.com/FastLED/FastLED
#include <TinyStepper_28BYJ_48.h>
#include <TrueRandom.h>
/* --------------------------------- LED -------------------------------------*/
#define NUM_LEDS 1          // Number of LEDS
#define DATA_PIN 11         // WS2812 pin number
CRGB leds[NUM_LEDS];        // init LEDs
uint8_t gHue = 0;           // Color wheel function Hue Value
uint8_t gHueDelta = 51;
uint8_t BLEVEL = 40;        // Brightness level //255 Divisors: 3,5,15,17,51,85
uint8_t BRIGHT = 20;        // Brightness level 2nd
uint8_t COLOR = 0;          // Color Wheel value from 0-255
uint8_t NUMBER = 0;
/* ---------------------------- Stepper Motor --------------------------------*/
// pin assignments          IN1/IN2/IN3/IN4
const uint8_t StepAPins[4] = {3,4,5,6};
const uint8_t StepBPins[4] = {7,8,9,10};
const uint8_t StepCPins[4] = {11,12,13,A0};
const int HALL_SENSOR_PIN = 2;
bool stopFlag = false;
bool calFlag = false;
long rannum = 0;
uint8_t currentDigit = 0;
long moveSteps = 0;
uint8_t i = 0;
uint8_t NUM = 0;
uint8_t OBJ = 0;
uint8_t STEPPER= 0;
//                           0,   1,   2,   3,   4,   5,    6 ,   7 ,   8 ,   9 ,     - ,  cal
const long absolutPos[12] = {0, -170, -340, -512, -682, -852, -1024, -1194, -1364, -1536, -1706};
// create the stepper motor object
TinyStepper_28BYJ_48 stepA;
TinyStepper_28BYJ_48 stepB;
TinyStepper_28BYJ_48 stepC;
//TinyStepper_28BYJ_48 stepD;

/* ----------------------------- General Vars---------------------------------*/
bool wait = 0;
/*---------------------------- Define Functions ------------------------------*/
uint8_t getTemp(); // Needs to be defined here to be used by on_Temperature();
void printDigits(int digits);
void moveNsteps(long steps, int obj);
void Cali(int STEPPER);
/*------------------ Define available CmdMessenger commands ------------------*/
enum {
  LED_ON,
  LEDS_OFF,
  DIS_NUM,
  CAL,
  error,
  return_str
};
/* Initialize CmdMessenger -- this should match PyCmdMessenger instance */
const int BAUD_RATE = 14400; // Baud Rates: 9600, 14400, 19200,
CmdMessenger c = CmdMessenger(Serial, ',', ';', '/');

/*------------------------ CmdMessenger functions ----------------------------*/
void on_LED_ON(void) {
  NUMBER = c.readBinArg<int>();   // LED Number
  COLOR = c.readBinArg<int>();    // Color
  BRIGHT = c.readBinArg<int>();   // Brightness
  leds[NUMBER].setHue(COLOR);
  LEDS.setBrightness(BRIGHT);
  FastLED.show();
  c.sendCmd(return_str, "LED_SET");
}

void on_LEDS_OFF(void) {
  for (uint8_t i = 0; i < NUM_LEDS; i++) {
    leds[i].setRGB(0, 0, 0);
  }
  FastLED.show();
  c.sendCmd(return_str, "LEDS_OFF");
}

void on_DIS_NUM(void) {
  NUM = c.readBinArg<int>();
  OBJ = c.readBinArg<int>();
  moveSteps = absolutPos[NUM];
  moveNsteps(moveSteps, OBJ);
  c.sendCmd(return_str, "NUM_SET");
}

void on_CAL(void) {
  STEPPER = c.readBinArg<int>();
  Cali(STEPPER);
  c.sendCmd(return_str, "CALIB");
}
void on_unknown_command(void) {
  c.sendCmd(error, "Command without callback.");
}

void attach_callbacks(void) { // Attach callbacks for CmdMessenger commands
  c.attach(LED_ON, on_LED_ON);
  c.attach(LEDS_OFF, on_LEDS_OFF);
  c.attach(DIS_NUM, on_DIS_NUM);
  c.attach(CAL, on_CAL);
  c.attach(on_unknown_command);
}

/*---------------------------- regular functions -----------------------------*/
void rangeRainbow() { // rainbow colors offet by 7 across each led
  fill_rainbow( leds, NUM_LEDS, gHue, gHueDelta); // FastLED's built-in rainbow generator
}

void clearLEDS() {
  for (uint8_t i = 0; i < NUM_LEDS; i++) {
    leds[i].setRGB(0, 0, 0);
  }
  FastLED.show();
}

void RandomNumberTest(){
  //has display switch to random digits
  rannum = TrueRandom.random(0, 9);
  moveSteps = absolutPos[rannum];
  Serial.print("rannum: ");
  Serial.println(rannum);
  Serial.print("moveSteps: ");
  Serial.println(moveSteps);
  moveNsteps(moveSteps,1);
  moveNsteps(moveSteps,2);
  moveNsteps(moveSteps,3);
  delay(2000);
}

void StepCal(int Stepper) { // Finds Zero point on number wheel with hall effect sensor
  stopFlag = false;
  calFlag = false;
  if(Stepper == 1) {
    stepA.setCurrentPositionInSteps(0);
    stepA.setupMoveInSteps(2048 * 10);
  }
  if(Stepper == 2) {
    stepB.setCurrentPositionInSteps(0);
    stepB.setupMoveInSteps(2048 * 10);
  }
  if(Stepper == 3) {
      stepC.setCurrentPositionInSteps(0);
      stepC.setupMoveInSteps(2048 * 10);
  }

  while (calFlag == false && stopFlag == false) {
    if (stopFlag != true) {
      if(Stepper == 1)stepA.processMovement();
      if(Stepper == 2)stepB.processMovement();
      if(Stepper == 3)stepC.processMovement();
    }

    if ((digitalRead(A1) == LOW) && calFlag == false && Stepper == 1) {
      //Serial.println("HAll A1");
      calFlag = true;
      stopFlag = true;
    }
    if ((digitalRead(A2) == LOW) && calFlag == false && Stepper == 2) {
      //Serial.println("HAll A2");
      calFlag = true;
      stopFlag = true;
    }
    if ((digitalRead(A3) == LOW) && calFlag == false && Stepper == 3) {
      //Serial.println("HAll A3");
      calFlag = true;
      stopFlag = true;
    }
    if (stopFlag == true && calFlag == true) {
      //Serial.println("Stop");
      if(Stepper == 1)stepA.setupStop();
      if(Stepper == 2)stepB.setupStop();
      if(Stepper == 3)stepC.setupStop();
    }
  }
  stepA.setCurrentPositionInSteps(0);
  stepB.setCurrentPositionInSteps(0);
  stepC.setCurrentPositionInSteps(0);
}

void moveNsteps(long steps, int obj) { // move to posistion n
  if(obj == 1) {
    stepA.setupMoveInSteps(steps);
    while (!stepA.motionComplete()) {
      stepA.processMovement();
    }
  }
  if(obj == 2) {
    stepB.setupMoveInSteps(steps);
    while (!stepB.motionComplete()) {
      stepB.processMovement();
    }
  }
  if(obj == 3) {
    stepC.setupMoveInSteps(steps);
    while (!stepC.motionComplete()) {
      stepC.processMovement();
    }
  }
  stepA.setupStop();
  stepB.setupStop();
  stepC.setupStop();
  //Serial.println("move complete");
}

void Cali(int STEPPER){
  long offset = 512; // offset for all three stepper motors
  if(STEPPER == 1)stepA.setCurrentPositionInSteps(0);
  if(STEPPER == 2)stepB.setCurrentPositionInSteps(0);
  if(STEPPER == 3)stepC.setCurrentPositionInSteps(0);
  moveNsteps(256,STEPPER);// move so magnet is not over hall effect
  StepCal(STEPPER);// calibrate zero point
  delay(100);
  if(STEPPER == 1) moveNsteps(-64,1);// fine tune zero cal
  if(STEPPER == 2) moveNsteps(-40,2);// fine tune zero cal
  if(STEPPER == 3) moveNsteps(-64,3);// fine tune zero cal
  if(STEPPER == 1) stepA.setCurrentPositionInSteps(0);
  if(STEPPER == 2) stepB.setCurrentPositionInSteps(0);
  if(STEPPER == 3) stepC.setCurrentPositionInSteps(0);
  delay(100);
  moveNsteps(offset,STEPPER);// move to "0"
  delay(100);
  if(STEPPER == 1)stepA.setCurrentPositionInSteps(0);
  if(STEPPER == 2)stepB.setCurrentPositionInSteps(0);
  if(STEPPER == 3)stepC.setCurrentPositionInSteps(0);
  delay(100);
}
/*------------------------------- Setup & Main -------------------------------*/
void setup() {
  //pinMode(HALL_SENSOR_PIN, INPUT);
  pinMode(A0, INPUT); // Stepper motor 3 init4
  pinMode(A1, INPUT); // Limit switch 1
  pinMode(A2, INPUT); // Limit switch 2
  pinMode(A3, INPUT); // Limit switch 3
  Serial.begin(BAUD_RATE);
  attach_callbacks(); // CmdMessenger
  FastLED.addLeds<WS2811, DATA_PIN, RGB>(leds, NUM_LEDS); // use NEOPIXEL for WS2812
  LEDS.setBrightness(BRIGHT);
  clearLEDS();
  FastLED.show();
  // Stepper motor init
  stepA.connectToPins(StepAPins[0], StepAPins[1], StepAPins[2], StepAPins[3]);
  stepB.connectToPins(StepBPins[0], StepBPins[1], StepBPins[2], StepBPins[3]);
  stepC.connectToPins(StepCPins[0], StepCPins[1], StepCPins[2], StepCPins[3]);
  // Speed:600 Acceleration:400
  long spd = 600;
  long acc = 400;
  stepA.setSpeedInStepsPerSecond(spd);
  stepA.setAccelerationInStepsPerSecondPerSecond(acc);
  stepB.setSpeedInStepsPerSecond(spd);
  stepB.setAccelerationInStepsPerSecondPerSecond(acc);
  stepC.setSpeedInStepsPerSecond(spd);
  stepC.setAccelerationInStepsPerSecondPerSecond(acc);
}

void loop() {
  c.feedinSerialData();
  //RandomNumberTest();
}
